const fs = require('fs');
// раннер nuxt-cli
const {run} = require('@nuxt/cli/dist/cli.js');

// аргументы комманды -  = [путьКНоде, путьКИсполняемогуФайлу, ...аргументы]
const args = process.argv.slice(2);
// путь к сокету типа ../sock/pid.sock
const socketFile = process.env.PWD + '/' + args[0];
// набор опций к комманде типа ['--spa', '--config-file="..."', ....]
const options = args.slice(1);

// если сокет файл уде есть - сносим
if (fs.existsSync(socketFile)) {
  fs.unlinkSync(socketFile);
}

let iterationsLimit = 500;
// Я не нашел как узнать что nuxt уже создал сокет файл, так что долбимся по интервалу
setInterval(() => fs.exists(socketFile,exist => {
  // передолбились (15 секунд прошло, вероятно где то проблема, раз nuxt не поднялся, в норме это сотые доли секунды)
  if (iterationsLimit <= 0) return;
  // если сокет появился - chmod его на 777 и стопарим интервал
  exist && fs.chmod(socketFile, 0o777, () => iterationsLimit = 0);
  iterationsLimit--;
}), 30);
// выполняем нативно комманду
run(['start', `--unix-socket=${socketFile}`, ...options]);